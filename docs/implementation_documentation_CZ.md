# Implementační dokumentace modulu *Counters*

## Záměr

Modul slouží ke sčítání chodců.

## Vstupní data


### Data aktivně stahujeme

Popis dat, jak je pomocí cronu a integration-engine stahujeme. Pokud jsou data stahována z více zdrojů, každý zdroj je popsán zvlášť.

#### *dataSourceEcoCounter*

- zdroj dat
  - url https://apieco.eco-counter-tools.com/api/1.0/site
- formát dat
  - protokol HTTP GET, Bearer token
  - datový typ json
  - odkaz na validační schéma již deprecated mongoose validačních schéma v 1.0.3 verzi bicycle counters
  - příklad vstupních dat
- (volitelně) vstupní parametry, hlavičky, atd.
```json
[
    {
        "id": 100047647,
        "name": "Praha - Podolí",
        "domain": "PRAHA  ",
        "latitude": 50.05949686563836,
        "longitude": 14.419192187464143,
        "userType": 7,
        "timezone": "(UTC+01:00) Europe/Prague;DST",
        "interval": 15,
        "sens": 0,
        "installationDate": "2018-09-14T00:00:00+0200",
        "photos": ["15380385573882.jpg", "15380385575921.jpg", "15380385604483.jpg", "15380385611850.jpg"],
        "counter": "COM18090165",
        "channels": [
            {
                "id": 101047647,
                "name": "Praha Pěší do centra",
                "domain": "PRAHA  ",
                "latitude": 50.05949686563836,
                "longitude": 14.419192187464143,
                "userType": 1,
                "timezone": "(UTC+01:00) Europe/Prague;DST",
                "interval": 15,
                "sens": 1,
                "installationDate": "2018-09-14T00:00:00+0200",
                "photos": null,
                "counter": "COM18090165",
                "channels": null
            },
            {
                "id": 102047647,
                "name": "Praha Pěší z centra",
                "domain": "PRAHA  ",
                "latitude": 50.05949686563836,
                "longitude": 14.419192187464143,
                "userType": 1,
                "timezone": "(UTC+01:00) Europe/Prague;DST",
                "interval": 15,
                "sens": 2,
                "installationDate": "2018-09-14T00:00:00+0200",
                "photos": null,
                "counter": "COM18090165",
                "channels": null
            }
        ]
    }
]
```

- frekvence stahování
  - cron definice 0 15 6 * * *
- název rabbitmq fronty dataplatform.counters.refreshEcoCounterDataInDB
  - obsahuje prázdnou zprávu, aktualizuje číselníky a pošle zprávu do queue pro update měření

  #### *dataSourceEcoCounterMeasurements*

- zdroj dat
  - url https://apieco.eco-counter-tools.com/api/1.0/data/site/:id?begin=:from&end=:to&step=:step&complete=:complete
- formát dat
  - protokol HTTP GET, Bearer token
  - datový typ json
  - odkaz na validační schéma již deprecated mongoose validačních schéma v 1.0.3 verzi bicycle counters
  - příklad vstupních dat
- (volitelně) vstupní parametry, hlavičky, atd.
```json
[
  {
     "date": "2020-01-16T02:30:00+0000",
     "counts": 0,
     "status": 0
  },
  {
     "date": "2020-01-16T02:45:00+0000",
     "counts": 0,
     "status": 0
  }
]
```

- název rabbitmq fronty dataplatform.counters.updateEcoCounter
  - obsahuje json zprávu s parametry, popsáno níže

## Zpracování dat / transformace

Popis transformace a případného obohacení dat. Zaměřeno hlavně na workery a jejich metody.

### *Counters Worker*

Stručný popis workeru.

#### *refreshEcoCounterDataInDB*

- vstupní rabbitmq fronta
  - název dataplatform.counters.refreshEcoCounterDataInDB
- (volitelně) závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
  - název dataplatform.counters.updateEcoCounter
- datové zdroje
  - výčet datových zdrojů dataSourceEcoCounter
- transformace
  - odkaz na [transformaci dat](./../src/integration-engine/EcoCounterTransformation.ts)
- data modely
  - výčet datových modelů, kam se data ukládájí
  - countersLocationsModel  - číselník lokací v db tabulce counters_locations
  - countersDirectionsModel - číselník seznorů v db tabulce counters_directions

#### *refreshEcoCounterDataInDB*

- vstupní rabbitmq fronta
  - název dataplatform.counters.updateEcoCounter
  - json zpráva s propertky
    - category - vždy pedestrian
    - directions - channel id s prefixem "ecoCounter-"
    - id - channel id
    - locations_id - id lokace s prefixem "ecoCounter-"
- datové zdroje
  - výčet datových zdrojů dataSourceEcoCounter
- transformace
  - odkaz na [transformaci dat](./../src/integration-engine/EcoCounterTransformation.ts)
- data modely
  - výčet datových modelů, kam se data ukládájí
  - countersDetectionsModel - záznamy o pohybech osob v 15 minutových intervalech, ukládá se do db tabulky counters_detections

## Uložení dat

Popis ukládání dat.

### Obecné

- typ databáze
  - PSQL,
- databázové schéma
  - public
- retence dat
  - data jsou zachována
- ERD
- ![counters er diagram](assets/counters_erd.png)


## Output API

Modul nemá output API.

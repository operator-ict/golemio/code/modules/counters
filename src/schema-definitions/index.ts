import Sequelize from "@golemio/core/dist/shared/sequelize";

// Locations
const outputLocationsSDMA: Sequelize.ModelAttributes<any> = {
    id: {
        primaryKey: true,
        type: Sequelize.STRING,
    },
    lat: Sequelize.DECIMAL,
    lng: Sequelize.DECIMAL,
    name: Sequelize.STRING,
    route: Sequelize.STRING,
    vendor: Sequelize.STRING,
    vendor_id: Sequelize.STRING,

    // Audit database fields
    create_batch_id: { type: Sequelize.BIGINT }, // ID of input batch
    created_at: { type: Sequelize.DATE }, // Time of insertion
    created_by: { type: Sequelize.STRING(150) }, // Author or name of Worker of inserion
    update_batch_id: { type: Sequelize.BIGINT }, // ID last input batch which caused update
    updated_at: { type: Sequelize.DATE }, // Time of update
    updated_by: { type: Sequelize.STRING(150) }, // Author or name of Worker of update
};

// Directions
const outputDirectionsSDMA: Sequelize.ModelAttributes<any> = {
    id: {
        primaryKey: true,
        type: Sequelize.STRING,
    },
    locations_id: Sequelize.STRING,
    name: Sequelize.STRING,
    vendor_id: Sequelize.STRING,

    // Audit database fields
    create_batch_id: { type: Sequelize.BIGINT }, // ID of input batch
    created_at: { type: Sequelize.DATE }, // Time of insertion
    created_by: { type: Sequelize.STRING(150) }, // Author or name of Worker of inserion
    update_batch_id: { type: Sequelize.BIGINT }, // ID last input batch which caused update
    updated_at: { type: Sequelize.DATE }, // Time of update
    updated_by: { type: Sequelize.STRING(150) }, // Author or name of Worker of update
};

// Detections
const outputDetectionsSDMA: Sequelize.ModelAttributes<any> = {
    category: { type: Sequelize.STRING(100) },
    directions_id: {
        primaryKey: true,
        type: Sequelize.STRING,
    },
    locations_id: {
        primaryKey: true,
        type: Sequelize.STRING,
    },
    measured_from: {
        primaryKey: true,
        type: Sequelize.BIGINT,
    },
    measured_to: Sequelize.BIGINT,
    value: Sequelize.INTEGER,

    // Audit database fields
    create_batch_id: { type: Sequelize.BIGINT }, // ID of input batch
    created_at: { type: Sequelize.DATE }, // Time of insertion
    created_by: { type: Sequelize.STRING(150) }, // Author or name of Worker of inserion
    update_batch_id: { type: Sequelize.BIGINT }, // ID last input batch which caused update
    updated_at: { type: Sequelize.DATE }, // Time of update
    updated_by: { type: Sequelize.STRING(150) }, // Author or name of Worker of update
};

const forExport = {
    detections: {
        name: "CountersDetections",
        outputSequelizeAttributes: outputDetectionsSDMA,
        pgTableName: "counters_detections",
    },
    directions: {
        name: "CountersDirections",
        outputSequelizeAttributes: outputDirectionsSDMA,
        pgTableName: "counters_directions",
    },
    locations: {
        name: "CountersLocations",
        outputSequelizeAttributes: outputLocationsSDMA,
        pgTableName: "counters_locations",
    },
};

export { forExport as Counters };

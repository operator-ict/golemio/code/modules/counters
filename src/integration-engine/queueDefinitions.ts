import { config } from "@golemio/core/dist/integration-engine/config";
import { IQueueDefinition } from "@golemio/core/dist/integration-engine/queueprocessors";
import { CountersWorker } from "#ie/CountersWorker";

const queueDefinitions: IQueueDefinition[] = [
    {
        name: "Counters",
        queuePrefix: config.RABBIT_EXCHANGE_NAME + "." + "counters",
        queues: [
            {
                name: "refreshEcoCounterDataInDB",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: CountersWorker,
                workerMethod: "refreshEcoCounterDataInDB",
            },
            {
                name: "updateEcoCounter",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: CountersWorker,
                workerMethod: "updateEcoCounter",
            },
        ],
    },
];

export { queueDefinitions };

<div align="center">
<p>
    <a href="https://operatorict.cz">
        <img src="https://gitlab.com/operator-ict/golemio/code/modules/core/-/raw/development/.assets/oict_logo.png" alt="oict" width="100px" height="100px" />
    </a>
    <a href="https://golemio.cz">
        <img src="https://gitlab.com/operator-ict/golemio/code/modules/core/-/raw/development/.assets/golemio_logo.png" alt="golemio" width="100px" height="100px" />
    </a>
</p>

<h1>@golemio/counters</h1>

<p>
    <a href="https://gitlab.com/operator-ict/golemio/code/modules/counters/commits/master">
        <img src="https://gitlab.com/operator-ict/golemio/code/modules/counters/badges/master/pipeline.svg" alt="pipeline">
    </a>
    <a href="https://gitlab.com/operator-ict/golemio/code/modules/counters/commits/master">
        <img src="https://gitlab.com/operator-ict/golemio/code/modules/counters/badges/master/coverage.svg" alt="coverage">
    </a>
    <a href="./LICENSE">
        <img src="https://img.shields.io/npm/l/@golemio/counters" alt="license">
    </a>
</p>

<p>
    <a href="#installation">Installation</a> · <a href="./docs">Documentation</a> · <a href="https://operator-ict.gitlab.io/golemio/code/modules/counters">TypeDoc</a>
</p>
</div>

> :warning: This module has been merged with the flow module and is **no longer maintained**. Please refer [here](https://gitlab.com/operator-ict/golemio/code/modules/flow).

This module is intended for use with Golemio services. Refer [here](https://gitlab.com/operator-ict/golemio/code/modules/core/-/blob/development/README.md) for further information on usage, local development and more.

## Installation

The APIs may be unstable. Therefore, we recommend to install this module as an exact version.

```bash
# Latest version
yarn add --exact @golemio/counters@latest

# Development version
yarn add --exact @golemio/counters@dev
```

<!-- ## Description -->

<!-- Insert module-specific info here -->

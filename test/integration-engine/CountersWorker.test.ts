import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { config } from "@golemio/core/dist/integration-engine/config";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { CountersWorker } from "#ie/CountersWorker";

describe("CountersWorker", () => {
    let worker: CountersWorker;
    let sandbox: SinonSandbox;
    let queuePrefix: string;
    let testData: number[];
    let testTransformedData: Record<string, any>;
    let testMeasurementsData: number[];
    let testEcoCounterMeasurementsTransformedData: any[];

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub(),
            })
        );

        config.datasources = {
            CountersEcoCounterTokens: {
                OICT: "",
            },
            BicycleCountersEcoCounterMeasurements: "",
            BicycleCountersEcoCounter: "",
        };

        testData = [1, 2];
        testTransformedData = {
            directions: [
                { vendor_id: "103047647", id: "ecoCounter-103047647", locations_id: "ecoCounter-100047647" },
                { vendor_id: "104047647", id: "ecoCounter-104047647", locations_id: "ecoCounter-100047647" },
            ],
            directionsPedestrians: [
                { vendor_id: "103047647", id: "ecoCounter-103047647", locations_id: "ecoCounter-100047647" },
                { vendor_id: "104047647", id: "ecoCounter-104047647", locations_id: "ecoCounter-100047647" },
            ],
            locations: [{ vendor_id: "BC_BS-BMZL" }, { vendor_id: "BC_AT-STLA" }],
            locationsPedestrians: [{ vendor_id: "BC_BS-BMZL" }, { vendor_id: "BC_AT-STLA" }],
        };
        testMeasurementsData = [1, 2];
        testEcoCounterMeasurementsTransformedData = [
            {
                directions_id: null,
                locations_id: null,
                measured_from: new Date().valueOf(),
                measured_to: new Date().valueOf(),
                value: 1,
            },
        ];

        worker = new CountersWorker();

        sandbox.stub(worker["dataSourceEcoCounter"], "getAll").callsFake(() => Promise.resolve(testData));
        sandbox.stub(worker["dataSourceEcoCounterMeasurements"], "getAll").callsFake(() => Promise.resolve(testMeasurementsData));

        sandbox.stub(worker["ecoCounterTransformation"], "transform").callsFake(() => testTransformedData as any);
        sandbox
            .stub(worker["ecoCounterMeasurementsTransformation"], "transform")
            .callsFake(() => testEcoCounterMeasurementsTransformedData as any);
        sandbox.stub(worker, "sendMessageToExchange" as any).resolves();

        sandbox.stub(worker["countersLocationsModel"], "save");
        sandbox.stub(worker["countersDirectionsModel"], "save");
        sandbox.stub(worker["countersDetectionsModel"], "saveBySqlFunction");

        queuePrefix = config.RABBIT_EXCHANGE_NAME + "." + "counters";
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should calls the correct methods by refreshEcoCounterDataInDB method", async () => {
        await worker.refreshEcoCounterDataInDB({});
        sandbox.assert.calledOnce(worker["dataSourceEcoCounter"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["ecoCounterTransformation"].transform as SinonSpy);
        sandbox.assert.calledWith(worker["ecoCounterTransformation"].transform as SinonSpy, testData);
        sandbox.assert.calledOnce(worker["countersLocationsModel"].save as SinonSpy);
        sandbox.assert.calledOnce(worker["countersDirectionsModel"].save as SinonSpy);
        // @ts-ignore
        sandbox.assert.callCount(worker["sendMessageToExchange"], 2);
        testTransformedData.directions.map((f: Record<string, any>) => {
            sandbox.assert.calledWith(
                // @ts-ignore
                worker["sendMessageToExchange"],
                "workers." + queuePrefix + ".updateEcoCounter",
                JSON.stringify({
                    category: "pedestrian",
                    directions_id: f.id,
                    id: f.vendor_id,
                    locations_id: f.locations_id,
                })
            );
        });
        sandbox.assert.callOrder(
            worker["dataSourceEcoCounter"].getAll as SinonSpy,
            worker["ecoCounterTransformation"].transform as SinonSpy,
            worker["countersLocationsModel"].save as SinonSpy,
            worker["countersDirectionsModel"].save as SinonSpy,
            // @ts-ignore
            worker["sendMessageToExchange"]
        );
    });

    it("should calls the correct methods by updateEcoCounter method (different geo)", async () => {
        await worker.updateEcoCounter({
            content: Buffer.from(
                JSON.stringify({
                    category: "pedestrian",
                    directions_id: "ecoCounter-103047647",
                    id: "103047647",
                    locations_id: "ecoCounter-100047647",
                })
            ),
        });

        sandbox.assert.calledOnce(worker["dataSourceEcoCounterMeasurements"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["ecoCounterMeasurementsTransformation"].transform as SinonSpy);
        sandbox.assert.calledWith(worker["ecoCounterMeasurementsTransformation"].transform as SinonSpy, testMeasurementsData);

        sandbox.assert.calledOnce(worker["countersDetectionsModel"].saveBySqlFunction as SinonSpy);
    });
});
